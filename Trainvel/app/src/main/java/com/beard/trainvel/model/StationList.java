package com.beard.trainvel.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

/**
 * Created by D.Andreichykov on 9/25/2014.
 */
@Root
public class StationList implements Serializable {

	@ElementList (entry = "objStation", inline = true)
    private List<Station> ArrayOfObjStation;

    public List<Station> getStations() {
        return ArrayOfObjStation;
    }

    public void setStations(List<Station> arrayOfObjStation) {
	    ArrayOfObjStation = arrayOfObjStation;
    }
}
