package com.beard.trainvel.net;

/**
 * Created by D.Andreichykov on 9/26/2014.
 */
public class GetStationsByName extends RequestDescription {

    private static final String COMMAND = "getStationDataByNameXML";

    public GetStationsByName() {
        super(COMMAND);
    }

    public void setStationDesc(final String stationDesc) {
        addParamether(null, "StationDesc", stationDesc);
    }

    public void clearStationDesc() {
        removeParametherKey(null, "StationDesc");
    }

    public void setNumMins(final int min) {
        addParamether(null, "NumMins", String.valueOf(min));
    }

    public void clearNumMins() {
        removeParametherKey(null, "NumMins");
    }
}
