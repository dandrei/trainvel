package com.beard.trainvel.net;

/**
 * Created by D.Andreichykov on 9/26/2014.
 */
public class GetStationFilter extends RequestDescription {

	private static final String COMMAND = "getStationsFilterXML";

	public GetStationFilter() {
		super(COMMAND);
	}

	public void setStationText(final String text) {
		addParamether(null, "StationText", text);
	}

	public void clearStationText() {
		removeParametherKey(null, "StationText");
	}
}
