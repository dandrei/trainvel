package com.beard.trainvel;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beard.trainvel.model.Station;
import com.beard.trainvel.model.StationList;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by beard on 9/28/14.
 */
public class StationMapFragment extends Fragment implements IStationsHandle {

	public static final String EXTRA = "extra";

	static final LatLng DUBLIN = new LatLng(53.344, -6.267);

	private final Gson mGson = new Gson();
	private StationList mList;

	@Override
	public void setStations(StationList list) {
		mList = list;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View v = inflater.inflate(R.layout.activity_map_fragment, container, false);
		return v;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mMap = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.my_map)).getMap();

		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(DUBLIN, 13));
		mMap.animateCamera(CameraUpdateFactory.zoomTo(8), 2000, null);

		mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
			@Override
			public void onInfoWindowClick(Marker marker) {
				getActivity().startActivity(new Intent(getActivity(), StationDetailsActivity.class).putExtra(StationDetailsActivity.EXTRA,
						mGson.toJson(mMarkerList.get(marker))));
			}
		});
		initMap();
	}

	private void initMap() {
		for (final Station station : mList.getStations()) {
			final Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(station.getStationLatitude(), station.getStationLongitude())).title(station.getStationDesc()).
					snippet(station.getStationCode()));
			mMarkerList.put(marker, station);
		}
	}

	private GoogleMap mMap;
	private Map<Marker, Station> mMarkerList = new HashMap<Marker, Station>();
}
