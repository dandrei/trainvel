package com.beard.trainvel.net;

import com.beard.trainvel.model.LocationType;

/**
 * Created by D.Andreichykov on 9/26/2014.
 */
public class GetStations extends RequestDescription {

	private static final String COMMAND = "getAllStationsXML";

	public GetStations() {
		super(COMMAND);
	}

	public void setLocationType(final LocationType type) {
		addParamether("WithStationType", "StationType", type.toString());
	}

	public void clearLocationType() {
		removeParametherKey("WithStationType", "StationType");
	}
}
