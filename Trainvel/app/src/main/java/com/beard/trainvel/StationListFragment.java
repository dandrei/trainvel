package com.beard.trainvel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.beard.trainvel.model.Station;
import com.beard.trainvel.model.StationList;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by beard on 9/28/14.
 */
public class StationListFragment extends Fragment implements IStationsHandle {

	public static final String EXTRA = "extra";

	private ListView mList;
	private final Gson mGson = new Gson();
	private StationList mStations;

	@Override
	public void setStations(final StationList list) {
		mStations = list;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActivity().setTitle(R.string.stations_information);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.activity_stations, null);
		mList = (ListView) view.findViewById(android.R.id.list);
		mList.setAdapter(new StationAdapter(mStations.getStations()));
		mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				view.getContext().startActivity(new Intent(view.getContext(), StationDetailsActivity.class).putExtra(StationDetailsActivity.EXTRA, mGson.toJson(((StationAdapter) adapterView.getAdapter()).getItem(i))));
			}
		});
		return view;
	}

	private static final class StationAdapter extends BaseAdapter {

		private final List<Station> mStationList;

		public StationAdapter(List<Station> list) {
			mStationList = list;
		}

		@Override
		public int getCount() {
			return mStationList.size();
		}

		@Override
		public Station getItem(int i) {
			return mStationList.get(i);
		}

		@Override
		public long getItemId(int i) {
			return getItem(i).getStationId();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final Holder holder;
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(parent.getContext());
				convertView = inflater.inflate(R.layout.item_list_station, parent, false);
				holder = new Holder();
				holder.title = (TextView) convertView.findViewById(R.id.title);
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}
			final Station station = getItem(position);
			holder.title.setText(station.getStationDesc());
			return convertView;
		}

		private static final class Holder {
			TextView title;
		}
	}
}
