package com.beard.trainvel.model;

import java.io.Serializable;

/**
 * Created by D.Andreichykov on 9/25/2014.
 */
public class TrainMovement implements Serializable {

    public enum StopType {
        C, /*Current*/
        N /*Next*/;
    }
    private static final String TIME_FORMAT = "HH:mm:ss";
    private static final String DATE_FORMAT = "dd MMM yyyy";

    private String TrainCode;//>E109 </TrainCode>
    private String TrainDate;   // DATE_FORMAT   //>21 Dec 2011</TrainDate>
    private String LocationCode;//>PMNCK</LocationCode>
    private String LocationFullName;//>Portmarnock</LocationFullName>
    private int LocationOrder;//>2</LocationOrder>
    private LocationType LocationType;//>S</LocationType>
    private String TrainOrigin;//>Malahide</TrainOrigin>
    private String TrainDestination;//>Greystones</TrainDestination>
    private String ScheduledArrival;    //  TIME_FORMAT //>10:33:30</ScheduledArrival>
    private String ScheduledDeparture;    //  TIME_FORMAT //>10:34:00</ScheduledDeparture>
    private String ExpectedArrival;    //  TIME_FORMAT //>10:33:54</ExpectedArrival>
    private String ExpectedDeparture;    //  TIME_FORMAT //>10:34:18</ExpectedDeparture>
    private String Arrival;    //  TIME_FORMAT //>10:33:48</Arrival>
    private String Departure;    //  TIME_FORMAT //>10:35:18</Departure>
    private int AutoArrival;//>1</AutoArrival>
    private int AutoDepart;//>1</AutoDepart>
    private StopType StopType;//>-</StopType>

    public int getAutoArrival() {
        return AutoArrival;
    }

    public int getAutoDepart() {
        return AutoDepart;
    }

    public int getLocationOrder() {
        return LocationOrder;
    }

    public String getArrival() {
        return Arrival;
    }

    public String getDeparture() {
        return Departure;
    }

    public String getExpectedArrival() {
        return ExpectedArrival;
    }

    public String getExpectedDeparture() {
        return ExpectedDeparture;
    }

    public String getLocationCode() {
        return LocationCode;
    }

    public String getLocationFullName() {
        return LocationFullName;
    }

    public LocationType getLocationType() {
        return LocationType;
    }

    public String getScheduledArrival() {
        return ScheduledArrival;
    }

    public String getScheduledDeparture() {
        return ScheduledDeparture;
    }

    public StopType getStopType() {
        return StopType;
    }

    public String getTrainCode() {
        return TrainCode;
    }

    public String getTrainDate() {
        return TrainDate;
    }

    public String getTrainDestination() {
        return TrainDestination;
    }

    public String getTrainOrigin() {
        return TrainOrigin;
    }

    public void setArrival(String arrival) {
        Arrival = arrival;
    }

    public void setAutoArrival(int autoArrival) {
        AutoArrival = autoArrival;
    }

    public void setAutoDepart(int autoDepart) {
        AutoDepart = autoDepart;
    }

    public void setDeparture(String departure) {
        Departure = departure;
    }

    public void setExpectedArrival(String expectedArrival) {
        ExpectedArrival = expectedArrival;
    }

    public void setExpectedDeparture(String expectedDeparture) {
        ExpectedDeparture = expectedDeparture;
    }

    public void setLocationCode(String locationCode) {
        LocationCode = locationCode;
    }

    public void setLocationFullName(String locationFullName) {
        LocationFullName = locationFullName;
    }

    public void setLocationOrder(int locationOrder) {
        LocationOrder = locationOrder;
    }

    public void setLocationType(LocationType locationType) {
        LocationType = locationType;
    }

    public void setScheduledArrival(String scheduledArrival) {
        ScheduledArrival = scheduledArrival;
    }

    public void setScheduledDeparture(String scheduledDeparture) {
        ScheduledDeparture = scheduledDeparture;
    }

    public void setStopType(StopType stopType) {
        StopType = stopType;
    }

    public void setTrainCode(String trainCode) {
        TrainCode = trainCode;
    }

    public void setTrainDate(String trainDate) {
        TrainDate = trainDate;
    }

    public void setTrainDestination(String trainDestination) {
        TrainDestination = trainDestination;
    }

    public void setTrainOrigin(String trainOrigin) {
        TrainOrigin = trainOrigin;
    }
}
