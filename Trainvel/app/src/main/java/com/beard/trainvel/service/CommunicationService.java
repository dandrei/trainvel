package com.beard.trainvel.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

public class CommunicationService extends Service {

	private static AsyncHttpClient client = new AsyncHttpClient();
	private final IBinder mBinder = new MyBinder();

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	public void request(final String url, final OnReceive receiver) {
		client.get(this, url, null, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, String responseString) {
				super.onSuccess(statusCode, headers, responseString);
				if (statusCode == HttpStatus.SC_OK) {
					receive(receiver, responseString);
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				if (statusCode == HttpStatus.SC_OK) {
					receive(receiver, responseString);
				} else {
					super.onFailure(statusCode, headers, responseString, throwable);
					fail(receiver);
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				fail(receiver);
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
				super.onFailure(statusCode, headers, throwable, errorResponse);
				fail(receiver);
			}

			private void receive(final OnReceive r, final String s) {
				if (r != null) {
					r.onReceive(s);
				}
			}

			private void fail(final OnReceive r) {
				if (r != null) {
					r.onFail();
				}
			}
		});

	}

	public class MyBinder extends Binder {
		public CommunicationService getService() {
			return CommunicationService.this;
		}
	}
}
