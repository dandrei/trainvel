package com.beard.trainvel.net;

import com.beard.trainvel.model.LocationType;

/**
 * Created by D.Andreichykov on 9/26/2014.
 */
public class GetTrains extends RequestDescription {

	private static final String COMMAND = "getCurrentTrainsXML";

	public GetTrains() {
		super(COMMAND);
	}

	public void setLocationType(final LocationType type) {
		addParamether("WithTrainType", "TrainType", type.toString());
	}

	public void clearLocationType() {
		removeParametherKey("WithTrainType", "TrainType");
	}
}
