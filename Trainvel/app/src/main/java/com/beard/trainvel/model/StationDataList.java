package com.beard.trainvel.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

/**
 * Created by D.Andreichykov on 9/25/2014.
 */

@Root
public class StationDataList implements Serializable {

	@ElementList(entry = "objStationData", inline = true, empty = false, name = "ArrayOfObjStationData")
	private List<StationData> ArrayOfObjStationData;

	public List<StationData> getArrayOfObjStationData() {
		return ArrayOfObjStationData;
	}

	public void setArrayOfObjStationData(List<StationData> arrayOfObjStationData) {
		ArrayOfObjStationData = arrayOfObjStationData;
	}
}
