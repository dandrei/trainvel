package com.beard.trainvel.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by D.Andreichykov on 9/25/2014.
 */
public class StationFilterList implements Serializable {

    private List<StationFilter> ArrayOfObjStationFilter;

    public List<StationFilter> getArrayOfObjStationFilter() {
        return ArrayOfObjStationFilter;
    }

    public void setArrayOfObjStationFilter(List<StationFilter> arrayOfObjStationFilter) {
        ArrayOfObjStationFilter = arrayOfObjStationFilter;
    }
}
