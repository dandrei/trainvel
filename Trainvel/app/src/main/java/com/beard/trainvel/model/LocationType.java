package com.beard.trainvel.model;

/**
 * Created by D.Andreichykov on 9/26/2014.
 */
public enum LocationType {
    O, /*Origin*/
    S, /*Stop*/
    T, /*TimingPoint */
    D /*Destination */;
}
