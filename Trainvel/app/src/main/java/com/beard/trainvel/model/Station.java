package com.beard.trainvel.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Created by D.Andreichykov on 9/25/2014.
 */
@Root
public class Station implements Serializable {

	@Element(required = false)
	private long StationId;
	@Element(required = false)
	private String StationCode;
	@Element(required = false)
	private double StationLongitude;
	@Element(required = false)
	private double StationLatitude;
	@Element(required = false)
	private String StationAlias;
	@Element(required = false)
	private String StationDesc;

	public void setStationAlias(String stationAlias) {
		StationAlias = stationAlias;
	}

	public void setStationCode(String stationCode) {
		StationCode = stationCode;
	}

	public void setStationDesc(String stationDesc) {
		StationDesc = stationDesc;
	}

	public void setStationId(long stationId) {
		StationId = stationId;
	}

	public void setStationLatitude(double stationLatitude) {
		StationLatitude = stationLatitude;
	}

	public void setStationLongitude(double stationLongitude) {
		StationLongitude = stationLongitude;
	}

	public double getStationLatitude() {
		return StationLatitude;
	}

	public double getStationLongitude() {
		return StationLongitude;
	}

	public long getStationId() {
		return StationId;
	}

	public String getStationAlias() {
		return StationAlias;
	}

	public String getStationCode() {
		return StationCode;
	}

	public String getStationDesc() {
		return StationDesc;
	}
}
