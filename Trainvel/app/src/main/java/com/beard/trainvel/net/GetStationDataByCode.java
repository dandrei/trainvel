package com.beard.trainvel.net;

/**
 * Created by D.Andreichykov on 9/26/2014.
 */
public class GetStationDataByCode extends RequestDescription {

	private static final String COMMAND = "getStationDataByCodeXML";

	boolean codeSet, minSet = false;
	private int mMin;
	private String mCode;

	public GetStationDataByCode() {
		super(COMMAND);
	}

	public void setNumMins(final int min) {
		minSet = true;
		mMin = min;
	}

	public void clearNumMins() {
		minSet = false;
	}

	public void setStationCode(final String code) {
		codeSet = true;
		mCode = code;
	}

	public void clearStationCode() {
		codeSet = false;
	}

	@Override
	public String build() {
		final String param = minSet ? "WithNumMins" : null;
		if (codeSet) {
			addParamether(param, "StationCode", mCode);
		}
		if (minSet) {
			addParamether(param, "NumMins", String.valueOf(mMin));
		}
		return super.build();
	}
}
