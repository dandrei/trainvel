package com.beard.trainvel;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.beard.trainvel.model.LocationType;
import com.beard.trainvel.model.StationList;
import com.beard.trainvel.net.GetStationDataByCode;
import com.beard.trainvel.net.GetStationFilter;
import com.beard.trainvel.net.GetStations;
import com.beard.trainvel.net.GetStationsByName;
import com.beard.trainvel.net.GetTrainMovements;
import com.beard.trainvel.net.GetTrains;
import com.beard.trainvel.net.RequestDescription;
import com.beard.trainvel.service.OnReceive;
import com.google.gson.Gson;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.Calendar;


public class MainActivity extends OnlineActivity {

	private ProgressBar mProgress;
	private final Gson mGson = new Gson();

	private void showProgress() {
		mProgress.setVisibility(View.VISIBLE);
	}

	private void hideProgress() {
		mProgress.setVisibility(View.GONE);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mProgress = (ProgressBar) findViewById(R.id.progress);
		findViewById(R.id.more).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {

				RequestDescription d = new GetStations();
				final String request = d.build();
				Log.e("TEST", request);
				boolean requested = request(request, new OnReceive() {
					@Override
					public void onReceive(String data) {
						Serializer serializer = new Persister();
						StationList model = null;
						try {
							model = serializer.read(StationList.class, data);
						} catch (Exception e) {
							e.printStackTrace();
						}
						hideProgress();
						Log.e("TEST", request);
						if (model != null) {
//								view.getContext().startActivity(new Intent(view.getContext(), MapFragmentActivity.class).putExtra(MapFragmentActivity.EXTRA, mGson.toJson(model)));
							view.getContext().startActivity(new Intent(view.getContext(), StationsActivity.class).putExtra(StationsActivity.EXTRA, mGson.toJson(model)));
						}
					}

					@Override
					public void onFail() {
						hideProgress();
					}
				});
				if (requested) {
					showProgress();
				}
			}
		});
		test();
	}

	public void test() {
		RequestDescription description = new GetStations();
		String url = "http://api.irishrail.ie/realtime/realtime.asmx/getAllStationsXML";
		testDescriptor(description, "getAllStationsXML ", url);

		GetStations d1 = new GetStations();
		d1.setLocationType(LocationType.D);
		url = "http://api.irishrail.ie/realtime/realtime.asmx/getAllStationsXML_WithStationType?StationType=D";
		testDescriptor(d1, "getAllStationsXML_WithStationType ", url);

		GetTrains d2 = new GetTrains();
		url = "http://api.irishrail.ie/realtime/realtime.asmx/getCurrentTrainsXML";
		testDescriptor(d2, "getCurrentTrainsXML ", url);

		GetTrains d3 = new GetTrains();
		d3.setLocationType(LocationType.D);
		url = "http://api.irishrail.ie/realtime/realtime.asmx/getCurrentTrainsXML_WithTrainType?TrainType=D";
		testDescriptor(d3, "getCurrentTrainsXML_WithTrainType ", url);

		GetStationsByName d4 = new GetStationsByName();
		d4.setStationDesc("Bayside");
		url = "http://api.irishrail.ie/realtime/realtime.asmx/getStationDataByNameXML?StationDesc=Bayside";
		testDescriptor(d4, "getStationDataByNameXML ", url);

		GetStationsByName d5 = new GetStationsByName();
		d5.setStationDesc("Bayside");
		d5.setNumMins(20);
		url = "http://api.irishrail.ie/realtime/realtime.asmx/getStationDataByNameXML?StationDesc=Bayside&NumMins=20";
		testDescriptor(d5, "getStationDataByNameXML 2 params ", url);

		GetStationDataByCode d6 = new GetStationDataByCode();
		d6.setStationCode("mhide");
		url = "http://api.irishrail.ie/realtime/realtime.asmx/getStationDataByCodeXML?StationCode=mhide";
		testDescriptor(d6, "getStationDataByCodeXML", url);

		GetStationDataByCode d7 = new GetStationDataByCode();
		d7.setStationCode("mhide");
		d7.setNumMins(20);
		url = "http://api.irishrail.ie/realtime/realtime.asmx/getStationDataByCodeXML_WithNumMins?StationCode=mhide&NumMins=20";
		testDescriptor(d7, "getStationDataByCodeXML 2 params", url);

		GetStationFilter d8 = new GetStationFilter();
		d8.setStationText("br");
		url = "http://api.irishrail.ie/realtime/realtime.asmx/getStationsFilterXML?StationText=br";
		testDescriptor(d8, "getStationsFilterXML", url);

		GetTrainMovements d9 = new GetTrainMovements();
		d9.setTrainId("e109");
		Calendar c = Calendar.getInstance();
		c.set(2011, Calendar.DECEMBER, 21);
		d9.setTrainDate(c.getTime());
		url = "http://api.irishrail.ie/realtime/realtime.asmx/getTrainMovementsXML?TrainId=e109&TrainDate=21 dec 2011";
		testDescriptor(d9, "getTrainMovementsXML", url);


//		CommunicationService.getService().request("http://google.com");

	}

	private void testDescriptor(final RequestDescription d, final String commandLog, final String url) {
		String built = d.build();
		if (url.equals(built)) {
			Log.v("TEST", commandLog + " Passed");
		} else {
			Log.e("TEST", commandLog + ": " + built);
		}
	}

	@Override
	protected void onConnected() {

	}

	@Override
	protected void onDisconnected() {

	}
}
