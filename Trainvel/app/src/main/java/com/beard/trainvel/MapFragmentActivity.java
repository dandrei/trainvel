package com.beard.trainvel;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.beard.trainvel.model.Station;
import com.beard.trainvel.model.StationList;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;


public class MapFragmentActivity extends FragmentActivity {

	public static final String EXTRA = "extra";

	static final LatLng DUBLIN = new LatLng(53.344, -6.267);

	private final Gson mGson = new Gson();
	private StationList mList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map_fragment);
		mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.my_map)).getMap();

		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(DUBLIN, 13));
		mMap.animateCamera(CameraUpdateFactory.zoomTo(8), 2000, null);
		mList = mGson.fromJson(getIntent().getStringExtra(EXTRA), StationList.class);
		assert (mList == null);
		assert (mList.getStations() == null);
		initMap();


	}

	private void initMap() {
		for (final Station station : mList.getStations()) {
			final Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(station.getStationLatitude(), station.getStationLongitude())).title(station.getStationDesc()).
							snippet(station.getStationCode()));
		}
	}

	private GoogleMap mMap;

}
