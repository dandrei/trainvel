package com.beard.trainvel.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Created by D.Andreichykov on 9/25/2014.
 */

@Root
public class StationData implements Serializable {

	private static final String SERVERTIME_FORMAT = "yyyy-MM-ddTHH:mm:ss:SSS";
	private static final String TIME_FORMAT = "HH:mm:ss";
	private static final String TIME_FORMAT_NO_SECONDS = "HH:mm";
	private static final String DATE_FORMAT = "dd MMM yyyy";

	@Element(required = false)
	private String Servertime; // SERVERTIME_FORMAT //>2014-09-25T15:27:28.953</Servertime>
	@Element(required = false)
	private String Traincode;//>E923 </Traincode>

	@Element(required = false)
	private String Stationfullname;//>Bayside</Stationfullname>
	@Element(required = false)
	private String Stationcode;//>BYSDE</Stationcode>
	@Element(required = false)
	private String Querytime;   // TIME_FORMAT  //>15:27:28</Querytime>
	@Element(required = false)
	private String Traindate;   //  DATE_FORMAT //>25 Sep 2014</Traindate>
	@Element(required = false)
	private String Origin;//>Bray</Origin>
	@Element(required = false)
	private String Destination;//>Howth</Destination>
	@Element(required = false)
	private String Origintime;  // TIME_FORMAT_NO_SECONDS   //>15:55</Origintime>
	@Element(required = false)
	private String Destinationtime; // TIME_FORMAT_NO_SECONDS   //>17:03</Destinationtime>
	@Element(required = false)
	private String Status;//>No Information</Status>
	@Element(required = false)
	private String Lastlocation;///>        //  LastLocation (Arrived|Departed StationName)
	@Element(required = false)
	private int Duein;//>90</Duein>
	@Element(required = false)
	private int Late;//>0</Late>
	@Element(required = false)
	private String Exparrival; // TIME_FORMAT_NO_SECONDS   //>16:56</Exparrival>
	@Element(required = false)
	private String Expdepart; // TIME_FORMAT_NO_SECONDS   //>16:57</Expdepart>
	@Element(required = false)
	private String Scharrival; // TIME_FORMAT_NO_SECONDS   //>16:56</Scharrival>
	@Element(required = false)
	private String Schdepart; // TIME_FORMAT_NO_SECONDS   //>16:57</Schdepart>
	@Element(required = false)
	private String Direction;//>Northbound</Direction>
	@Element(required = false)
	private String Traintype;//>DART</Traintype>
	@Element(required = false)
	private LocationType Locationtype;//>S</Locationtype>


	public String getDirection() {
		return Direction;
	}

	public int getDuein() {
		return Duein;
	}

	public int getLate() {
		return Late;
	}

	public String getDestination() {
		return Destination;
	}

	public String getDestinationtime() {
		return Destinationtime;
	}

	public String getExparrival() {
		return Exparrival;
	}

	public String getExpdepart() {
		return Expdepart;
	}

	public String getLastlocation() {
		return Lastlocation;
	}

	public LocationType getLocationtype() {
		return Locationtype;
	}

	public String getOrigin() {
		return Origin;
	}

	public String getOrigintime() {
		return Origintime;
	}

	public String getQuerytime() {
		return Querytime;
	}

	public String getScharrival() {
		return Scharrival;
	}

	public String getSchdepart() {
		return Schdepart;
	}

	public String getServertime() {
		return Servertime;
	}

	public String getStationcode() {
		return Stationcode;
	}

	public String getStationfullname() {
		return Stationfullname;
	}

	public String getStatus() {
		return Status;
	}

	public String getTraincode() {
		return Traincode;
	}

	public String getTraindate() {
		return Traindate;
	}

	public String getTraintype() {
		return Traintype;
	}

	public void setDirection(String direction) {
		Direction = direction;
	}

	public void setDestination(String destination) {
		Destination = destination;
	}

	public void setDestinationtime(String destinationtime) {
		Destinationtime = destinationtime;
	}

	public void setDuein(int duein) {
		Duein = duein;
	}

	public void setExparrival(String exparrival) {
		Exparrival = exparrival;
	}

	public void setExpdepart(String expdepart) {
		Expdepart = expdepart;
	}

	public void setLastlocation(String lastlocation) {
		Lastlocation = lastlocation;
	}

	public void setLate(int late) {
		Late = late;
	}

	public void setLocationtype(LocationType locationtype) {
		Locationtype = locationtype;
	}

	public void setOrigin(String origin) {
		Origin = origin;
	}

	public void setOrigintime(String origintime) {
		Origintime = origintime;
	}

	public void setQuerytime(String querytime) {
		Querytime = querytime;
	}

	public void setScharrival(String scharrival) {
		Scharrival = scharrival;
	}

	public void setSchdepart(String schdepart) {
		Schdepart = schdepart;
	}

	public void setServertime(String servertime) {
		Servertime = servertime;
	}

	public void setStationcode(String stationcode) {
		Stationcode = stationcode;
	}

	public void setStationfullname(String stationfullname) {
		Stationfullname = stationfullname;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public void setTraincode(String traincode) {
		Traincode = traincode;
	}

	public void setTraindate(String traindate) {
		Traindate = traindate;
	}

	public void setTraintype(String traintype) {
		Traintype = traintype;
	}
}
