package com.beard.trainvel.model;

import java.util.Locale;

/**
 * Created by D.Andreichykov on 9/25/2014.
 */
public class TrainPosition {

    public static final String SERVER_DATE_FORMAT = "dd MMM yyyy";
    public static final Locale SERVER_LOCALE = Locale.ENGLISH;

    private String TrainStatus;
    private double TrainLatitude;
    private double TrainLongitude;
    private String TrainCode;
    private String TrainDate;
    private String PublicMessage;
    private String Direction;

    public double getTrainLatitude() {
        return TrainLatitude;
    }

    public double getTrainLongitude() {
        return TrainLongitude;
    }

    public String getTrainCode() {
        return TrainCode;
    }

    public String getTrainDate() {
        return TrainDate;
    }

    public String getTrainStatus() {
        return TrainStatus;
    }

    public void setTrainCode(String trainCode) {
        TrainCode = trainCode;
    }

    public void setTrainDate(String trainDate) {
        TrainDate = trainDate;
    }

    public void setTrainLatitude(double trainLatitude) {
        TrainLatitude = trainLatitude;
    }

    public void setTrainLongitude(double trainLongitude) {
        TrainLongitude = trainLongitude;
    }

    public void setTrainStatus(String trainStatus) {
        TrainStatus = trainStatus;
    }

    public String getDirection() {
        return Direction;
    }

    public void setDirection(String direction) {
        Direction = direction;
    }

    public String getPublicMessage() {
        return PublicMessage;
    }

    public void setPublicMessage(String publicMessage) {
        PublicMessage = publicMessage;
    }
}
