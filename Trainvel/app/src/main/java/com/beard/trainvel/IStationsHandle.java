package com.beard.trainvel;

import com.beard.trainvel.model.StationList;

/**
 * Created by beard on 9/28/14.
 */
public interface IStationsHandle {
	public void setStations(final StationList list);
}
