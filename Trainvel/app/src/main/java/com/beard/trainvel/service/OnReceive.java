package com.beard.trainvel.service;

/**
 * Created by beard on 9/27/14.
 */
public interface OnReceive {
	void onReceive(final String data);
	void onFail();
}
