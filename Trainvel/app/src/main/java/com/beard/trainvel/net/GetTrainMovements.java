package com.beard.trainvel.net;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by D.Andreichykov on 9/26/2014.
 */
public class GetTrainMovements extends RequestDescription {

	private static final String COMMAND = "getTrainMovementsXML";
	private static final String DATE_FORMAT = "dd MMM yyyy";

	public GetTrainMovements() {
		super(COMMAND);
	}

	public void setTrainId(final String trainId) {
		addParamether(null, "TrainId", trainId);
	}

	public void setTrainDate(final Date date) {
		addParamether(null, "TrainDate", new SimpleDateFormat(DATE_FORMAT).format(date));
	}

	public void clearTrainDate() {
		removeParametherKey(null, "TrainDate");
	}
}
