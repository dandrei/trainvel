package com.beard.trainvel;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.beard.trainvel.model.Station;
import com.beard.trainvel.model.StationData;
import com.beard.trainvel.model.StationDataList;
import com.beard.trainvel.net.GetStationDataByCode;
import com.beard.trainvel.service.OnReceive;
import com.google.gson.Gson;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.List;


public class StationDetailsActivity extends OnlineActivity {
	public static final String EXTRA = "extra";

	private final Gson mGson = new Gson();

	private ListView mList;
	private ProgressBar mProgress;
	private Station mStation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mStation = mGson.fromJson(getIntent().getStringExtra(EXTRA), Station.class);

		setContentView(R.layout.activity_station_details);
		findViewById(R.id.refresh).setOnClickListener(mClickListener);
		mList = (ListView) findViewById(android.R.id.list);
		setTitle(mStation.getStationDesc());
		GuiUtils.setTextOrHide(findViewById(R.id.station_name), mStation.getStationDesc());
		GuiUtils.setTextOrHide(findViewById(R.id.station_code), mStation.getStationCode());

		mProgress = (ProgressBar) findViewById(android.R.id.progress);
		showProgress();
	}


	@Override
	protected void onConnected() {
		requestData();
	}

	@Override
	protected void onDisconnected() {

	}

	private void requestData() {
		showProgress();
		GetStationDataByCode d = new GetStationDataByCode();
		d.setNumMins(60);
		d.setStationCode(mStation.getStationCode());
		final String request = d.build();
		boolean requested = request(request, new OnReceive() {
			@Override
			public void onReceive(String data) {
				Serializer serializer = new Persister();
				StationDataList model = null;
				try {
					model = serializer.read(StationDataList.class, data);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (model != null && model.getArrayOfObjStationData() != null) {
					mList.setAdapter(new StationDataAdapter(model.getArrayOfObjStationData()));
				}
				hideProgress();
			}

			@Override
			public void onFail() {
				hideProgress();
			}
		});
		if (!requested) {
			hideProgress();
		}
	}

	private void showProgress() {
		mProgress.setVisibility(View.VISIBLE);
	}

	private void hideProgress() {
		mProgress.setVisibility(View.GONE);
	}

	private View.OnClickListener mClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			if (view.getId() == R.id.refresh) {
				requestData();
			}
		}
	};

	private static class StationDataAdapter extends BaseAdapter {

		private final List<StationData> mStationDataList;

		public StationDataAdapter(final List<StationData> list) {
			mStationDataList = list;
		}

		@Override
		public int getCount() {
			return mStationDataList.size();
		}

		@Override
		public StationData getItem(int i) {
			return mStationDataList.get(i);
		}

		@Override
		public long getItemId(int i) {
			return i;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final Holder holder;
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(parent.getContext());
				convertView = inflater.inflate(R.layout.item_list_station_details, parent, false);
				holder = new Holder();

				holder.mStationName = (TextView) convertView.findViewById(R.id.station_name);
				holder.mStationCode = (TextView) convertView.findViewById(R.id.station_code);
				holder.mLastUpdate = (TextView) convertView.findViewById(R.id.last_update);
				holder.mDestination = (TextView) convertView.findViewById(R.id.destination);
				holder.mDueIn = (TextView) convertView.findViewById(R.id.due_in);
				holder.mDepartureTime = (TextView) convertView.findViewById(R.id.departure_time);
				holder.mLate = (TextView) convertView.findViewById(R.id.late);
				holder.mStatus = (TextView) convertView.findViewById(R.id.status);
				holder.mDirection = (TextView) convertView.findViewById(R.id.direction);
				convertView.setTag(holder);
			} else {
				holder = (Holder) convertView.getTag();
			}
			final StationData station = getItem(position);

			GuiUtils.setTextOrHide(holder.mStationName, station.getStationfullname());
			GuiUtils.setTextOrHide(holder.mStationCode, station.getStationcode());
			GuiUtils.setTextOrHide(holder.mLastUpdate, station.getQuerytime());
			GuiUtils.setTextOrHide(holder.mDestination, station.getDestination());
			GuiUtils.setTextOrHide(holder.mDueIn, String.valueOf(station.getDuein()));
			GuiUtils.setTextOrHide(holder.mDepartureTime, station.getOrigintime());
			GuiUtils.setTextOrHide(holder.mLate, String.valueOf(station.getLate()));
			GuiUtils.setTextOrHide(holder.mStatus, station.getStatus());
			GuiUtils.setTextOrHide(holder.mDirection, station.getDirection());
			return convertView;
		}
	}

	private static final class Holder {
		private TextView mStationName, mStationCode, mLastUpdate, mDestination, mDueIn, mDepartureTime, mLate, mStatus, mDirection;
	}

}
