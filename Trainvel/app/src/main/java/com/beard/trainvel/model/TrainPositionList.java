package com.beard.trainvel.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by D.Andreichykov on 9/25/2014.
 */
public class TrainPositionList implements Serializable {

    private List<TrainPosition> ArrayOfObjTrainPositions;

    public List<TrainPosition> getStations() {
        return ArrayOfObjTrainPositions;
    }

    public void setArrayOfObjTrainPositions(List<TrainPosition> arrayOfObjTrainPositions) {
        ArrayOfObjTrainPositions = arrayOfObjTrainPositions;
    }

}