package com.beard.trainvel.net;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by D.Andreichykov on 9/26/2014.
 */
public abstract class RequestDescription {

    private static final String API_PROVIDER = "http://api.irishrail.ie/realtime/realtime.asmx";

    private final String mMethod;
    final Map<String, Map<String, String>> params = new HashMap<String, Map<String, String>>();

    public RequestDescription(final String method) {
        mMethod = method;
    }

    protected void addParamether(final String param, final String key, final String value) {
        if (key != null && value != null) {
	        final Map<String, String> data = params.get(param);
            if (data == null) {
                final Map<String, String> map = new HashMap<String, String>();
                map.put(key, value);
                params.put(param, map);
            } else {
                data.put(key, value);
            }
        } else {
            if (key != null) {
                removeParametherKey(param, key);
            } else {
                removeParamether(param);
            }
        }
    }

    protected void removeParamether(final String param) {
        if (param != null) {
            params.remove(param);
        }
    }

    protected void removeParametherKey(final String param, final String key) {
        if (param != null && key != null) {
            params.get(param).remove(key);
        }
    }

    public String build() {
        final StringBuilder builder = new StringBuilder(API_PROVIDER);
        if (mMethod != null) {
            builder.append("/").append(mMethod);
        }
        for (final Map.Entry<String, Map<String, String>> entry : params.entrySet()) {
            if (entry.getKey() != null) {
                builder.append("_").append(entry.getKey());
            }
            builder.append("?");
            for (final Iterator iit = entry.getValue().entrySet().iterator(); iit.hasNext(); ) {
                final Map.Entry<String, String> p = (Map.Entry) iit.next();
                builder.append(p.getKey()).append("=").append(p.getValue()).append(iit.hasNext() ? "&" : "");
            }
        }
        return builder.toString();
    }
}
