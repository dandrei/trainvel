package com.beard.trainvel;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

/**
 * Created by beard on 9/28/14.
 */
public class GuiUtils {

	public static final void setTextOrHide(final View view, final String text) {
		if (view != null) {
			if (TextUtils.isEmpty(text)) {
				view.setVisibility(View.GONE);
			} else {
				if(view instanceof TextView) {
					((TextView) view).setText(text);
					view.setVisibility(View.VISIBLE);
				}
			}
		}
	}
}
