package com.beard.trainvel;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TabHost;

import com.beard.trainvel.model.StationList;
import com.beard.trainvel.net.GetStations;
import com.beard.trainvel.net.RequestDescription;
import com.beard.trainvel.service.OnReceive;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;


public class ScheduleActivity extends OnlineActivity {

	private TabHost mTabHost;
	private StationList mStations = null;

	private Fragment[] mFragments = new Fragment[]{new StationListFragment(), new StationMapFragment()};
	private static final int LIST = 0, MAP = 1;
	private int mCurrentTab = -1;

	public static class DummyTabContent implements TabHost.TabContentFactory {
		private Context mContext;

		public DummyTabContent(Context context) {
			mContext = context;
		}

		@Override
		public View createTabContent(String tag) {
			View v = new View(mContext);
			return v;
		}
	}

	@Override
	protected void onConnected() {
		RequestDescription d = new GetStations();
		final String request = d.build();
		boolean requested = mCurrentTab == -1 && request(request, new OnReceive() {
			@Override
			public void onReceive(String data) {
				Serializer serializer = new Persister();
				try {
					mStations = serializer.read(StationList.class, data);
					initTab("list");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFail() {

			}
		});
	}

	@Override
	protected void onDisconnected() {
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_schedule);
		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup();

		TabHost.TabSpec t = mTabHost.newTabSpec("list");
		t.setIndicator("List");
		t.setContent(new DummyTabContent(getBaseContext()));
		mTabHost.addTab(t);
		TabHost.TabSpec m = mTabHost.newTabSpec("map");
		m.setIndicator("Map");
		m.setContent(new DummyTabContent(getBaseContext()));
		mTabHost.addTab(m);

		mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
			@Override
			public void onTabChanged(String s) {
				initTab(s);
			}
		});
	}

	private void initTab(final String tag) {
		android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
		android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
		for (Fragment f : mFragments) {
			if (f.isAdded()) ft.hide(f);
		}
		mCurrentTab = ("map".equals(tag) ? MAP : LIST);
		Fragment f = mFragments[mCurrentTab];
		if (f instanceof IStationsHandle) {
			((IStationsHandle) f).setStations(mStations);
		}
		if (f.isAdded()) {
			ft.show(f);
		} else {
			ft.add(android.R.id.tabcontent, f);
		}
		ft.commit();
	}

}