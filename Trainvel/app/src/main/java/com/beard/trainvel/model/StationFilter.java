package com.beard.trainvel.model;

import java.io.Serializable;

/**
 * Created by D.Andreichykov on 9/25/2014.
 */
public class StationFilter implements Serializable {

    private String StationDesc_sp;//>Broombridge</StationDesc_sp>
    private String StationDesc;//>Broombridge</StationDesc>
    private String StationCode;//>BBRDG</StationCode>

    public String getStationDesc() {
        return StationDesc;
    }

    public String getStationCode() {
        return StationCode;
    }

    public String getStationDesc_sp() {
        return StationDesc_sp;
    }

    public void setStationCode(String stationCode) {
        StationCode = stationCode;
    }

    public void setStationDesc(String stationDesc) {
        StationDesc = stationDesc;
    }

    public void setStationDesc_sp(String stationDesc_sp) {
        StationDesc_sp = stationDesc_sp;
    }
}
