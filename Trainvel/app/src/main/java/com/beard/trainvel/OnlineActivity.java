package com.beard.trainvel;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;

import com.beard.trainvel.service.CommunicationService;
import com.beard.trainvel.service.OnReceive;

/**
 * Created by beard on 9/28/14.
 */
public abstract class OnlineActivity extends FragmentActivity {

	private CommunicationService myService;

	public ServiceConnection myConnection = new ServiceConnection() {

		public void onServiceConnected(ComponentName className, IBinder binder) {
			myService = ((CommunicationService.MyBinder) binder).getService();
			onConnected();
		}

		public void onServiceDisconnected(ComponentName className) {
			myService = null;
			onDisconnected();
		}
	};

	@Override
	protected void onResume() {
		if (myService == null) {
			Intent intent = new Intent(this, CommunicationService.class);
			bindService(intent, myConnection, Context.BIND_AUTO_CREATE);
		}
		super.onResume();
	}

	@Override
	protected void onPause() {
		if (myService != null) {
			unbindService(myConnection);
			myService = null;
		}
		super.onPause();
	}

	protected boolean request(final String url, final OnReceive receiver) {
		if (myService != null) {
			myService.request(url, receiver);
			return true;
		}
		return false;
	}

	protected abstract void onConnected();
	protected abstract void onDisconnected();
}
