package com.tekadept.utest.app.tests;


import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;

import com.beard.trainvel.MainActivity;
import com.beard.trainvel.R;

import junit.framework.TestCase;

/**
 * Created by beard on 9/27/14.
 */
public class SimpleTestCase extends ActivityInstrumentationTestCase2<MainActivity> {

	private MainActivity mActivity;

	public SimpleTestCase(Class<MainActivity> activityClass) {
		super(activityClass);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();

		mActivity = getActivity();
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	@Override
	protected void runTest() throws Throwable {
		super.runTest();
	}

	@SmallTest
	public void testUi() {
		assertNull(mActivity.findViewById(R.id.helloworld));
		String s = "";
		assertNull(s);
	}
}
